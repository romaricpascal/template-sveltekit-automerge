import { Repo } from '@automerge/automerge-repo';
import { BroadcastChannelNetworkAdapter } from '@automerge/automerge-repo-network-broadcastchannel';
import { IndexedDBStorageAdapter } from '@automerge/automerge-repo-storage-indexeddb';
// eslint-disable-next-line import/no-unresolved -- For some reason cannot resolve svelte/store
import { writable } from 'svelte/store';

export async function load() {
  const repo = new Repo({
    network: [new BroadcastChannelNetworkAdapter()],
    storage: new IndexedDBStorageAdapter(),
  });

  let rootDocUrl = localStorage.rootDocUrl;
  if (!rootDocUrl) {
    const handle = repo.create();
    handle.change((doc) => {
      doc.notes = [];
    });
    localStorage.rootDocUrl = rootDocUrl = handle.url;
  }

  const state = automergeDocument(rootDocUrl, repo);

  await firstNonNullValue(state);

  return {
    repo,
    rootDocUrl,
    state,
  };
}

function firstNonNullValue(store) {
  return new Promise((resolve) => {
    store.subscribe((v) => {
      if (v !== void 0 && v !== null) resolve(v);
    });
  });
}

// Not Automerge'
function automergeDocument(documentId, repo) {
  const handle = repo.find(documentId);

  console.log(handle.docSync());

  const { set, subscribe } = writable(handle.docSync(), () => {
    const onChange = (h) => set(h.doc);
    handle.addListener('change', onChange);
    return () => handle.removeListener('change', onChange);
  });

  return {
    subscribe,
    handle,
    change: (fn) => {
      handle.change(fn);
    },
  };
}
