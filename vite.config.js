import { sveltekit } from '@sveltejs/kit/vite';
import { defineConfig } from 'vite';
import wasm from 'vite-plugin-wasm';
import topLevelAwait from 'vite-plugin-top-level-await';

// https://github.com/automerge/automerge-repo/blob/main/examples/automerge-repo-demo-counter-svelte/vite.config.ts
export default defineConfig({
  plugins: [wasm(), topLevelAwait(), sveltekit()],

  worker: {
    format: 'es',
    plugins: [wasm(), topLevelAwait()],
  },

  optimizeDeps: {
    // This is necessary because otherwise `vite dev` includes two separate
    // versions of the JS wrapper. This causes problems because the JS
    // wrapper has a module level variable to track JS side heap
    // allocations, and initializing this twice causes horrible breakage
    exclude: [
      '@automerge/automerge-wasm',
      '@automerge/automerge-wasm/bundler/bindgen_bg.wasm',
      '@syntect/wasm',
    ],
  },
});
