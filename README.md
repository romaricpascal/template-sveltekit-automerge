# Project template - SvelteKit + Automerge app

Project for tracking craft projects, including notes and pictures

## Table of contents

* [Planned features](#planned-features)
* [Contributing](#contributing)

## Planned features

```sh
npx tiged gitlab:romaricpascal/template-sveltekit-automerge
```

Then:

* \[ ] Update `template-sveltekit-automerge` inside `package.json` to actual library name

  ```sh
  (            
    PROJECT_NAME=NEW_PROJECT_NAME;
    sed -i'.bak' "s/template-sveltekit-automerge/$PROJECT_NAME/" package.json;
    rm package.json.bak
    	npm install
  )
  ```

* \[ ] Update `README.md` with relevant info

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md)
